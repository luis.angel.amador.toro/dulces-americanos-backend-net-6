﻿using dulces_back1.Data;
using dulces_back1.Models;
using dulces_back1.ModelsDto;
using dulces_back1.Services;
using Microsoft.AspNetCore.Mvc;

namespace dulces_back1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CheckoutController : Controller
    {
        private readonly DataContext _context;
        public CheckoutController(DataContext context)
        {
            _context = context;
        }
        [HttpPost]
        public async Task<ActionResult<ShipmentSuccessDto>> SendData([FromBody] ShipmentDetailsDto shipment)
        {
            var dbShipment = new DetallesEnvío();
            var successShipment = new ShipmentSuccessDto();
            dbShipment.Name = shipment.Name;
            dbShipment.LastName = shipment.LastName;
            dbShipment.RFC = shipment.RFC;
            dbShipment.Email = shipment.Email;
            dbShipment.Phone = shipment.Phone;
            dbShipment.IsPaid = 0;
            dbShipment.State = shipment.State;
            dbShipment.City = shipment.City;
            dbShipment.Adress = shipment.Adress;
            dbShipment.Notes = shipment.Notes;
            dbShipment.Settlement = shipment.Settlement;
            dbShipment.Adress = shipment.Adress;
            dbShipment.CP = shipment.CP;
            dbShipment.Total = shipment.Total;
            dbShipment.Shipment = shipment.Shipment;

            successShipment.FullName = dbShipment.Name + " " + dbShipment.LastName;
            successShipment.Phone = dbShipment.Phone;
            successShipment.Email = dbShipment.Email;
            successShipment.IsPaid = dbShipment.IsPaid;
            successShipment.Total = dbShipment.Total.ToString();
            EmailService email = new EmailService();
            email.SendEmailData(shipment.CartProducts.ToList(), shipment); ;
            if (dbShipment == null)
                return BadRequest("Error al subir los datos");
            _context.DetallesEnvio.Add(dbShipment);
            await _context.SaveChangesAsync();
            return Ok(successShipment);
        }
    }
}

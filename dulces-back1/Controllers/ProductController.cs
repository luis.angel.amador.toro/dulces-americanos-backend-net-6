﻿using dulces_back1.Data;
using dulces_back1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace dulces_back1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    
    public class ProductosController : ControllerBase
    {

        private readonly DataContext _context;
        public ProductosController(DataContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<List<Producto>>> Get()
        {

            return Ok(await _context.Productos.Include(x => x.Category).ToListAsync());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Producto>> Get(int id)
        {
            var producto = await _context.Productos.Include(x => x.Category).SingleOrDefaultAsync(x => x.Id == id);
            if (producto == null)
                return BadRequest("Error al encontrar el producto");
            return Ok(producto);
        }
        [HttpPost]
        public async Task<ActionResult<List<Producto>>> AddProduct(Producto heroe)
        {
            _context.Productos.Add(heroe);
            await _context.SaveChangesAsync();

            return Ok(await _context.Productos.ToListAsync());
        }
        [HttpPut]
        public async Task<ActionResult<List<Producto>>> UpdateProduct(Producto request)
        {
            var dbProducto = await _context.Productos.FindAsync(request.Id);
            if (dbProducto == null)
                return BadRequest("No se encontró el producto");

            dbProducto.Name = request.Name;
            dbProducto.Category = request.Category;
            dbProducto.ImageUrl = request.ImageUrl;
            dbProducto.Description = request.Description;
            dbProducto.Stock = request.Stock;
            dbProducto.Price = request.Price;

            await _context.SaveChangesAsync();
            return Ok(await _context.Productos.ToListAsync());
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<Producto>>> Delete(int id)
        {
            var dbProducto = await _context.Productos.FindAsync(id);
            if (dbProducto == null)
                return BadRequest("No se encontró el producto");

            _context.Productos.Remove(dbProducto);
            await _context.SaveChangesAsync();
            return Ok(await _context.Productos.ToListAsync());
        }
    }
}

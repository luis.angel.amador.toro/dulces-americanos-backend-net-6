﻿using dulces_back1.Data;
using dulces_back1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace dulces_back1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CPController : ControllerBase
    {
        private readonly DataContext _context;
        public CPController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("{codigoPostal}")]
        public async Task<ActionResult<List<CodigoPostal>>> Get(string codigoPostal)
        {
            var direcciones = _context.CodigosPostales.Where(x => x.CP == codigoPostal).ToList();
            return Ok(direcciones);
        }
    }
}

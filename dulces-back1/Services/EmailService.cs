﻿using dulces_back1.ModelsDto;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace dulces_back1.Services
{
    public class EmailService
    {
    public void SendEmailData(List<CartProductsDto> products, ShipmentDetailsDto order)
        {
            var expirationDate = DateTime.Now.AddDays(3);
            var dictionary = new Dictionary<string, string>
            {
                {"{correoElectronico}", order.Email},
                {"{estado}", order.State},
                {"{colonia}", order.Settlement},
                {"{cp}", order.CP },
                {"{municipio}", order.City },
                {"{telefono}", order.Phone},
                {"{domicilio}", order.Adress},
                {"{total}", order.Total.ToString()},
                {"{expiracion}", expirationDate.ToShortDateString()},
            };
            string rows = "";
            foreach(var i in products)
            {
                string row = @"<tr scope=""row""><td>" + i.Id + @"</td><td>" + i.Name + @"</td><td>" + i.Quantity + @"</td><td>" + @"$" + i.Quantity * i.Price + @".00" + @"</td></tr>";
                rows += row;
            }
            dictionary.Add("{rows}", rows);
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                string html = File.ReadAllText("C:\\Users\\Angel\\Desktop\\Dulces Americanos\\dulces-back1\\dulces-back1\\Assets\\template.html");
                foreach(var item in dictionary)
                {
                    html = html.Replace(item.Key, item.Value);
                }
                try
                {
                    using (MailMessage email = new MailMessage())
                    {
                        email.From = new MailAddress("tucorreoelectronico@gmail.com");
                        email.Subject = "DULCES AMERICANOS NOTIFICACIONES";
                        email.To.Add(order.Email);
                        email.Body = html;
                        email.IsBodyHtml = true;

                        using (SmtpClient smpt = new SmtpClient("smtp.gmail.com", 587))
                        {
                            smpt.Credentials = new NetworkCredential("tucorreoelectronico@gmail.com", "tucontraseña");
                            smpt.EnableSsl = true;
                            smpt.Send(email);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

    }
}

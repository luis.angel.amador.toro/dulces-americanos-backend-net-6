﻿namespace dulces_back1.Models
{
    public class CodigoPostal
    {
        public int Id { get; set; }
        public string CP { get; set; } = string.Empty;
        public string Municipio { get; set; } = string.Empty;
        public string Estado { get; set; } = string.Empty;
        public string Colonia { get; set; } = string.Empty;
}
}

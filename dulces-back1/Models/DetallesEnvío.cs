﻿namespace dulces_back1.Models
{
    public class DetallesEnvío
    {
        public int Id { get; set; }
        public int IsPaid { get; set; }
        public int Total { get; set; }
        public string Name { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Phone { get; set; } = string.Empty;
        public string Shipment { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string CP { get; set; } = string.Empty;
        public string State { get; set; } = string.Empty;
        public string RFC { get; set; } = string.Empty;
        public string City { get; set; } = string.Empty;
        public string Settlement { get; set; } = string.Empty;
        public string Adress { get; set; } = string.Empty;
        public string Notes { get; set; } = string.Empty;
    }
}

﻿namespace dulces_back1.Models
{
    public class Categoria
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UrlImage { get; set; }
        public List<Producto> Products { get; set; }
    }
}

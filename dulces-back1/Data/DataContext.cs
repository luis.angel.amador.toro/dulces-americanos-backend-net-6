﻿using dulces_back1.Models;
using Microsoft.EntityFrameworkCore;

namespace dulces_back1.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<CodigoPostal> CodigosPostales { get; set; }
        public DbSet<DetallesEnvío> DetallesEnvio { get; set; }

    }
}

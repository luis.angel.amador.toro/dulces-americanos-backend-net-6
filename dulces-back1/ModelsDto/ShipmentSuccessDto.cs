﻿namespace dulces_back1.ModelsDto
{
    public class ShipmentSuccessDto
    {
        public string FullName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Phone { get; set; } = string.Empty;
        public int IsPaid { get; set; }
        public string Total { get; set; } = string.Empty;
    }
}

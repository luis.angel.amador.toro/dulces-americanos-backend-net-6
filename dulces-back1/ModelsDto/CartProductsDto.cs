﻿namespace dulces_back1.ModelsDto
{
    public class CartProductsDto
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public int Quantity { get; set; } 
        public int Price { get; set; }
    }
}

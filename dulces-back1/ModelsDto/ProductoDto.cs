﻿namespace dulces_back1.ModelsDto
{
    public class ProductoDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public string Tags { get; set; }
        public int Stock { get; set; }
        public int Price { get; set; }
        public string ImageUrl { get; set; }
    }
}
